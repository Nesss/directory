﻿namespace TelephoneDirectory.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using TelephoneDirectory.DB;
    using TelephoneDrictory.Entities;

    /// <summary>
    /// PhoneService.
    /// </summary>
    public class PhoneService
    {
        private readonly PhoneDbContext db;

        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="context">context</param>
        public PhoneService(PhoneDbContext context)
        {
            db = context;
        }

        /// <summary>
        /// Get all phones method.
        /// </summary>
        public async Task<ActionResult<IEnumerable<Phone>>> GetAll()
        {
            return await db.Phones.ToListAsync();
        }

        /// <summary>
        /// Get one phone.
        /// </summary>
        /// <param name="id">id</param>
        public async Task<ActionResult<Phone>> Get(long id)
        {
            Phone phone = await db.Phones.FirstOrDefaultAsync(x => x.Id == id);
            return phone;
        }

        /// <summary>
        /// Add new phone.
        /// </summary>
        /// <param name="phone">phone</param>
        public async Task<ActionResult<Phone>> AddAsync(Phone phone)
        {
            db.Phones.Add(phone);
            await db.SaveChangesAsync();
            return phone;
        }

        /// <summary>
        /// Update phone.
        /// </summary>
        /// <param name="phone">phone</param>
        public async Task<ActionResult<Phone>> UpdateAsync(Phone phone)
        {
            db.Update(phone);
            await db.SaveChangesAsync();
            return phone;
        }

        /// <summary>
        /// Delete phone.
        /// </summary>
        /// <param name="id">id</param>
        public async Task<ActionResult<Phone>> DeleteAsync(long id)
        {
            Phone phone = db.Phones.FirstOrDefault(x => x.Id == id);
            db.Phones.Remove(phone);
            await db.SaveChangesAsync();
            return phone;
        }
    }
}
