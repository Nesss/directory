﻿namespace TelephoneDirectory.DB
{
    using Microsoft.EntityFrameworkCore;
    using TelephoneDrictory.Entities;

    /// <summary>
    /// PhoneDbContext.
    /// </summary>
    public class PhoneDbContext : DbContext
    {
        /// <summary>
        /// PhoneDbContext.
        /// </summary>
        /// <param name="options">options</param>
        public PhoneDbContext(DbContextOptions<PhoneDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Phones.
        /// </summary>
        public DbSet<Phone> Phones { get; set; }

        /// <summary>
        /// OnModelCreating.
        /// </summary>
        /// <param name="modelBuilder">modelBuilder</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Phone>().HasKey(p => p.Id);
            base.OnModelCreating(modelBuilder);
        }
    }
}
