﻿namespace TelephoneDirectory.API.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using TelephoneDirectory.Services;
    using TelephoneDrictory.Entities;

    /// <summary>
    /// Phone Controller.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneController : Controller
    {
        /// <summary>
        /// Get all phones method.
        /// </summary>
        /// <param name="service">service</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Phone>>> GetAll(
            [FromServices] PhoneService service)
        {
            return await service.GetAll();
        }

        /// <summary>
        /// Get one phone method.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="service">service</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<Phone>> Get(
            [FromRoute] long id,
            [FromServices] PhoneService service)
        {
            return await service.Get(id);
        }

        /// <summary>
        /// Create new phone.
        /// </summary>
        /// <param name="phone">phone</param>
        /// <param name="service">service</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Phone>> Create(
            [FromBody] Phone phone,
            [FromServices] PhoneService service)
        {
            return await service.AddAsync(phone);
        }

        /// <summary>
        /// Update old phone.
        /// </summary>
        /// <param name="phone">phone</param>
        /// <param name="service">service</param>
        [HttpPut]
        public async Task<ActionResult<Phone>> Update(
            [FromBody] Phone phone,
            [FromServices] PhoneService service)
        {
            return await service.UpdateAsync(phone);
        }

        /// <summary>
        /// Delete phone by id.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="service">service</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Phone>> Delete(
            [FromRoute] long id,
            [FromServices] PhoneService service)
        {
            return await service.DeleteAsync(id);
        }
    }
}
