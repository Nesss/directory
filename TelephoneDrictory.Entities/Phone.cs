﻿namespace TelephoneDrictory.Entities
{
    /// <summary>
    /// Phone class.
    /// </summary>
    public class Phone
    {
        /// <summary>
        /// Phone Id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Person Firstname and Lastname.
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// Person PhoneNumber.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Person Address.
        /// </summary>
        public string Address { get; set; }
    }
}
